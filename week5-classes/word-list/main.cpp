#include "word.h"

#include <iterator>

#include <set>

#include <iostream>

#include <algorithm>

int main() {
	using in=std::istream_iterator<Word>;
	std::set<Word> wlist { in { std::cin }, in { } };

	copy(wlist.begin(), wlist.end(), std::ostream_iterator<Word>{std::cout, "\n"});
}
