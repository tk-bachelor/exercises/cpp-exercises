#include "Word.h"

#include <string>

#include <cctype>

#include <stdexcept>

#include <algorithm>
#include <istream>

Word::Word(std::string word) {
	this->setWord(word);
}

void Word::setWord(std::string word) {
	if (this->isValidWord(word)) {
		this->word = word;
	} else {
		throw std::invalid_argument { "invalid word" };
	}
}

std::ostream& Word::print(std::ostream &os) const {
	os << word;
	return os;
}

std::istream& Word::read(std::istream &in) {
	char c { };
	std::string word { };
	while (in.good()) {
		if (std::isalpha(in.peek())) {
			in >> c;
			word += c;
		} else {
			if (word.empty()) {
				in.ignore();
			} else {
				break;
			}
		}
	}

	if (word.empty()) {
		in.setstate(std::ios::failbit | in.rdstate());
	} else {
		// nice to do: catch exception and set fail bit
		this->setWord(word);
	}
	return in;
}

bool Word::isValidWord(std::string word) const {
	if (word.length() == 0) {
		return false;
	}

	for (char c : word) {
		if (!std::isalpha(c)) {
			return false;
		}
	}

	return true;
}

bool Word::operator <(Word const &rword) const {
	std::string lvalue = this->word;
	std::string rvalue = rword.word;
	std::transform(lvalue.begin(), lvalue.end(), lvalue.begin(), ::tolower);
	std::transform(rvalue.begin(), rvalue.end(), rvalue.begin(), ::tolower);
	return lvalue < rvalue;
}

Word::~Word() {
	// TODO Auto-generated destructor stub
}

