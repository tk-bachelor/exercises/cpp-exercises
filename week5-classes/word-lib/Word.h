#ifndef SRC_WORD_H_
#define SRC_WORD_H_

#include <string>
#include <ostream>

class Word {
	std::string word;
public:
	Word() = default;
	Word(std::string word);
	void setWord(std::string word);
	std::ostream & print(std::ostream& os) const;
	std::istream & read(std::istream& in);
	bool operator<(Word const &rword) const;
	virtual ~Word();
private:
	bool isValidWord(std::string word) const;
};

inline std::ostream& operator<<(std::ostream& os, Word const & word) {
	return word.print(os);
}

inline std::istream& operator>>(std::istream& in, Word & word) {
	return word.read(in);
}

inline bool operator==(Word const &lword, Word const & rword) {
	return !(lword < rword) && !(rword < lword);
}

inline bool operator!=(Word const &lword, Word const &rword) {
	return !(lword == rword);
}

inline bool operator>(Word const &lword, Word const & rword) {
	return !(lword < rword) && (lword != rword);
}

inline bool operator<=(Word const &lword, Word const &rword) {
	return !(lword > rword);
}

inline bool operator>=(Word const &lword, Word const &rword){
	return !(lword < rword);
}

#endif /* SRC_WORD_H_ */
