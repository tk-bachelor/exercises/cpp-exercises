#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include "sayhello.h"
#include <sstream>

void testSayHello() {
	std::ostringstream out{};
	sayHello(out);
	ASSERT_EQUAL("Hello, world!\n", out.str());
}

bool runAllTests(int argc, char const *argv[]) {
	cute::suite s { };

	// add tests here
	s.push_back(CUTE(testSayHello));
	cute::xml_file_opener xmlfile(argc, argv);
	cute::xml_listener<cute::ide_listener<>> lis(xmlfile.out);
	auto runner = cute::makeRunner(lis, argc, argv);
	bool success = runner(s, "AllTests");
	return success;
}

int main(int argc, char const *argv[]) {
	return runAllTests(argc, argv) ? EXIT_SUCCESS : EXIT_FAILURE;
}
