//#include <boost/algorithm/string/case_conv.hpp>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <fstream>

#include <cctype>

bool isPalindrome(std::string word) {
//	std::string wordLower { word };
//	boost::algorithm::to_lower(wordLower);

//	return equal(wordLower.begin(), wordLower.end(), word.rbegin(), [](char l, char r){
//		return l == r;
//	});

	return equal(word.begin(), word.end(), word.rbegin(), [](char l, char r) {
		return ::tolower(l) == ::tolower(r);
	});
}

void readWords(std::ostream &out) {
	std::ifstream file("palindrome.txt");

	using inpitr = std::istream_iterator<std::string>;
	std::ostream_iterator<std::string> outitr { out, "\n" };

	out << "Palindromes\n";

	copy_if(inpitr { file }, inpitr { }, outitr, isPalindrome);
}

int main() {
	readWords(std::cout);
}

