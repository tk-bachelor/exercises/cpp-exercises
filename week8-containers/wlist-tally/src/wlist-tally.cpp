#include <Word.h>
#include <iostream>
#include <map>

int main() {
	std::map<Word, int> words { };

	Word w { };
	while (std::cin >> w) {
		++words[w];
	}

	for (auto const &p : words) {
		std::cout << p.first << " = " << p.second << "\n";
	}
	return 0;
}
