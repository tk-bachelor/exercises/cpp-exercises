#ifndef SRC_WORD_H_
#define SRC_WORD_H_

#include <string>
#include <ostream>

#include <algorithm>

#include <cctype>

class Word {
	std::string word;
public:
	Word() = default;
	explicit Word(std::string word);
	void setWord(std::string word);
	std::ostream & print(std::ostream& os) const;
	std::istream & read(std::istream& in);
	bool operator<(Word const &rword) const;
	bool operator==(Word const &rword) const;
private:
	bool isValidWord() const;
};

inline bool compareCaseless(char lchar, char rchar) {
	return tolower(lchar) == tolower(rchar);
}

inline std::ostream& operator<<(std::ostream& os, Word const & word) {
	return word.print(os);
}

inline std::istream& operator>>(std::istream& in, Word & word) {
	return word.read(in);
}

inline bool operator!=(Word const &lword, Word const &rword) {
	return !(lword == rword);
}

inline bool operator>(Word const &lword, Word const & rword) {
	return !(lword < rword) && (lword != rword);
}

inline bool operator<=(Word const &lword, Word const &rword) {
	return !(lword > rword);
}

inline bool operator>=(Word const &lword, Word const &rword){
	return !(lword < rword);
}

#endif /* SRC_WORD_H_ */
