#include "Word.h"

#include <string>

#include <stdexcept>

#include <algorithm>
#include <istream>

#include <cctype>

Word::Word(std::string word) :
		word { word } {
	if (!isValidWord()) {
		throw std::invalid_argument { "invalid word" };
	}
}

void Word::setWord(std::string word) {
	this->word = word;
	if (!this->isValidWord()) {
		throw std::invalid_argument { "invalid word" };
	}
}

std::ostream& Word::print(std::ostream &os) const {
	os << word;
	return os;
}

std::istream& Word::read(std::istream &in) {
	std::string word { };
	char c { };
	while (in.good()) {
		if (::isalpha(in.peek())) {
			in >> c;
			word += c;
		} else {
			if (word.empty()) {
				in.ignore();
			} else {
				this->setWord(word);
				break;
			}
		}
	}

	if (word.empty()) {
		in.setstate(std::ios::failbit);
	}
	return in;
}

bool Word::isValidWord() const {
	if (this->word.empty()) {
		return false;
	}
//
//	for (char c : this->word) {
//		if (!std::isalpha(c)) {
//			return false;
//		}
//	}
//	return true;
	return std::all_of(this->word.begin(), this->word.end(), ::isalpha);
}

bool Word::operator==(Word const & rword) const {
	return std::equal(this->word.begin(), this->word.end(), rword.word.begin(), [](char l, char r) {
		return std::tolower(l) == std::tolower(r);
	});
}

bool Word::operator <(Word const &rword) const {
	return std::lexicographical_compare(this->word.begin(), this->word.end(), rword.word.begin(), rword.word.end(), [](char l, char r) {
		return std::tolower(l) < std::tolower(r);
	});
}

