#include <algorithm>
#include <iostream>
#include <set>
#include <string>
#include <fstream>

#include <iterator>

std::set<std::string> makeAnagrams(std::string& word) {
	sort(word.begin(), word.end());
	std::set<std::string> anagrams { };

	anagrams.insert(word);
	while (std::next_permutation(word.begin(), word.end())) {
		anagrams.insert(word);
	}
	return anagrams;
}

void outputRealWordsWithLamda(std::ostream_iterator<std::string> out, std::set<std::string> const &list) {
	std::ifstream file { "palindrome.txt" };

	using inpitr = std::istream_iterator<std::string>;
	copy_if(inpitr { file }, inpitr { }, out, [&list](std::string const &s) {
		return list.count(s);
	});
}

int main() {
	std::string word { };
	std::cin >> word;

	auto list = makeAnagrams(word);

	std::ostream_iterator<std::string> out { std::cout, "\n" };
	copy(list.begin(), list.end(), out);

	// Real word anagram
	std::cout << "-----------------------------------\n";
	outputRealWordsWithLamda(out, list);
}
