#include "kwic.h"
#include "Word.h"

#include <vector>

#include <string>

#include <sstream>

#include <iterator>

#include <algorithm>

#include <set>

namespace kwic {

std::vector<std::vector<Word>> extractWordsLineByLine(std::istream &in) {
	std::vector<std::vector<Word>> lines { };

	std::string line { };
	while (std::getline(in, line)) {
		std::istringstream iss { line };

		std::istream_iterator<Word> wordItr { iss };
		std::istream_iterator<Word> eof { };
		std::vector<Word> words { wordItr, eof };

		// the above code is aquivalent to the following loop.
		//		Word w { };
		//		while (iss >> w) {
		//			words.push_back(Word{w});
		//		}

		lines.push_back(words);
	}

	return lines;
}

void printKeywords(std::ostream &out, std::set<std::vector<Word>> keywords) {
	std::for_each(keywords.begin(), keywords.end(), [&](const auto& line) {
		std::copy(line.begin(), line.end(), std::ostream_iterator<Word>(out, " "));
		out << "\n";
	});
}

void rotateKeywords(std::set<std::vector<Word>> &result, std::vector<std::vector<Word>> keywords) {
	for_each(keywords.begin(), keywords.end(), [&result](auto &line) {
		for(unsigned i=0; i<line.size(); i++) {
			std::rotate(line.begin(), line.begin() + 1, line.end());
			std::vector<Word> rotatedWords {line};
			result.insert(rotatedWords);
		}
	});
}

void kwic(std::istream & in, std::ostream & out) {
	std::vector<std::vector<Word>> keywords = extractWordsLineByLine(in);

	std::set<std::vector<Word>> result { };
	rotateKeywords(result, keywords);
	printKeywords(out, result);
}
}
