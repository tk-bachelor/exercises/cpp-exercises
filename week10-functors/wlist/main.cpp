#include "Word.h"
#include <iterator>

#include <set>

#include <iostream>

#include <algorithm>

#include <functional>

struct LengthComparator {
	bool operator()(Word const &lword, Word const &rword) const{
		int compare = lword.length() - rword.length();
		return (compare == 0) ? lword < rword : compare < 0;
	}
};

int main() {
	using in=std::istream_iterator<Word>;
	std::set<Word, LengthComparator> wlist { in { std::cin }, in { } };

	copy(wlist.begin(), wlist.end(), std::ostream_iterator<Word> { std::cout, "\n" });
}
