#include "wseventh.h"
#include "Word.h"
#include <istream>
#include <ostream>

#include <map>

#include <iterator>

#include <algorithm>

#include <utility>

#include <tuple>

#include <set>

unsigned constexpr LIMIT { 20 };

using wordOccurrences = std::map<Word, unsigned>;
using wordInpItr = std::istream_iterator<Word>;

wordOccurrences readInWords(std::istream &in) {
	wordOccurrences wc { };
	std::for_each(wordInpItr { in }, wordInpItr { }, [&wc](Word const &word) {
		wc[word]++;
	});
	return wc;
}

using countWordPair = std::pair<unsigned, Word>;

// count desc, words asc
struct CountWordPairComparator {
	bool operator()(countWordPair const &lhs, countWordPair const &rhs) const {
		// tie creates a tuple of lvalue references

		// lhs: Pair<2, again>
		// rhs: Pair<3, by>

		if(lhs.first == rhs.first){
			return lhs.second < rhs.second;
		}
		return rhs.second < lhs.second;
	}
};

using words = std::set<countWordPair, CountWordPairComparator>;

words sortWords(wordOccurrences wordCountMap) {
	words allWords { };

	// transform Map<Word, unsigned> to Set<Pair<unsigned, Word>> and order by given comparator
	std::transform(begin(wordCountMap), end(wordCountMap),
			std::inserter(allWords, begin(allWords)), [](auto const &wordCount) {
		std::make_pair(wordCount.second, wordCount.first);
	});
	return allWords;
}

std::ostream &operator<<(std::ostream &out, words::value_type const &countWord){
	out << countWord.second << ":" << countWord.first;
	return out;
}

void printFavouriteWords(std::ostream &out, words const &allWords, words::size_type limit) {
	auto nofWords = allWords.size();
	auto nofDisplayedElements = std::min(limit, nofWords);

	std::copy_n(begin(allWords), nofDisplayedElements, std::ostream_iterator<words::value_type> { out, "\n" });
}

void wfavorite(std::istream & in, std::ostream & out) {
	auto wordCountMap = readInWords(in);
	auto sortedWords = sortWords(wordCountMap);
	printFavouriteWords(out, sortedWords, LIMIT);
}
