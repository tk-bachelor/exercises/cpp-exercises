#ifndef INDEXABLESET_H_
#define INDEXABLESET_H_

#include <functional>
#include <set>
#include <stdexcept>

#include <iterator>

template<typename T, typename COMPARE = std::less<T>>
class indexableSet: public std::set<T, COMPARE> {
public:
	using std::set<T, COMPARE>::set;
//	using set_type = typename std::set<T, COMPARE>::size_type;
	using set_type = typename indexableSet::size_type;

	T const & operator[](int index) const {
		return at(index);
	}

	T const & at(int index) const {
		unsigned validIndex = getValidIndex(index);
		auto it = this->cbegin();
		return *std::next(it, validIndex);
	}

	T const & front() const {
		return this->at(0);
	}

	T const & back() const {
		return this->at(-1);
	}
private:
	// example: set size is 4
	// index 0 to 3 valid
	// index -4 to -1 valid
	// index >= 4 invalid
	// index < -4 invalid
	unsigned getValidIndex(int index) const {
		if (index < 0) {
			if (static_cast<set_type>(abs(index)) > this->size()) {
				throw std::out_of_range("Index is out of range");
			}
			// -1 -> 3
			// -2 -> 2
			// -3 -> 1
			// -4 -> 0
			return this->size() + index;
		} else {
			if (static_cast<set_type>(index) >= this->size()) {
				throw std::out_of_range("Index is out of range");
			}
			return index;
		}
	}
};

#endif /* INDEXABLESET_H_ */
