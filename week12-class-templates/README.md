# Class Templates

## Static or Dynamic Errors?

* When will a template be instantiated?
  * (x) At compile-time
  * At run-time
  * It depends, if the template is specified as static it is instatiated at compile-time otherwise at run-time

## Template Definition

* Where do you define a class template and its members?
  * In the header file?
  * In the source file?
  * (x) In both the header and the source file?

## Dependent Types ^

* If you have a type depending on a template parameter, how do you have to refer to that type?
  * use the keyword `typename` 
  * --> e.g. `using set_size_type = typename std::set<T, COMPARE>::size_type`
  * or shorter `using set_size_type = typename indexableSet::size_type;`

## Inheritance

* How do you refer to members inherited from a dependent type within a class template?
  * use `this->baseMember`

## Template Specialization

Is it neccessary for a (partial) specialization of a class template to provide the same interface (same member functions) as the unspecialized template?
  * yes