#include "primes.h"
#include <ostream>

#include <vector>

#include <numeric>

#include <iterator>

#include <algorithm>

bool is_prime(unsigned long long n) {
	if( n < 2){
		return false;
	}

	// n = 5
	// divisors[0,0,0]
	std::vector<unsigned long long> divisors(n-2);

	// divisors[2,3,4]
	std::iota(begin(divisors), end(divisors), 2);

	// 5 % 2 -> 1 OK
	// 5 % 3 -> 2 OK
	// 5 % 4 -> 1 OK
	// true: 5 is prime
	return std::none_of(begin(divisors), end(divisors), [n](auto div){
		return n % div == 0;
	});
}

void primes(std::ostream & out, unsigned long long limit) {
	std::vector<unsigned long long> primes(limit);
	// fill the range with subsequentially increasing values
	std::iota(begin(primes), end(primes), 1);

	std::copy_if(begin(primes), end(primes), std::ostream_iterator<unsigned long long> { out, "\n" }, is_prime);

}
