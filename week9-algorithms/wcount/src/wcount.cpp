#include "wcount.h"
#include "word.h"
#include <istream>

#include <iterator>

unsigned wcount(std::istream& in) {
	using worditr = std::istream_iterator<Word>;
	return std::distance(worditr{in}, worditr{});
}
