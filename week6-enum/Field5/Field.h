#ifndef FIELD_H_
#define FIELD_H_

#include <iosfwd>

struct Field5 {
	explicit constexpr Field5(unsigned x = 0u) :
			val { x % 5 } {
	}
	constexpr unsigned value() const {
		return val;
	}
	constexpr operator unsigned() const {
		return val;
	}
	constexpr bool operator==(Field5 const &r) const {
		return val == r.val;
	}
	constexpr bool operator!=(Field5 const &r) const {
		return !(*this == r);
	}

	constexpr Field5 operator+=(Field5 const &r) {
		val = (val + r.value()) % 5;
		return *this;
	}

	constexpr Field5 operator*=(Field5 const&r) {
		val = (val * r.value()) % 5;
		return *this;
	}

	constexpr Field5 operator+(Field5 const &r) const {
		return Field5 { val + r.val };
	}

	constexpr Field5 operator-(Field5 const &r) const {
		return Field5 { val - r.val };
	}

	constexpr Field5 operator*(Field5 const &r) const {
		return Field5 { val * r.val };
	}

	constexpr Field5 operator/(Field5 const &r) const {
		if(r.val == 0){
			return r;
		}
		return Field5 { val / r.val };
	}

private:
	Field5 multiplicativeInverse() const {
		// 2*3 % 5 = 1
		if (val == 2)
			return Field5 { 3 };
		if (val == 3)
			return Field5 { 2 };
		// 1*1 % 5 = 1
		// 4*4 % 5 = 1
		return *this;
	}
	unsigned val;
};

namespace R5 {
constexpr Field5 operator"" _R5(unsigned long long v) {
	return Field5 { static_cast<unsigned>(v % 5) };
}
}
std::ostream& operator <<(std::ostream& out, const Field5& r);

#endif /* FIELD_H_ */
