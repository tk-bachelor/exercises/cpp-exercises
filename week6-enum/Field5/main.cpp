#include "Field.h"
#include <iostream>

int main() {
	Field5 f5 { 11 };
	Field5 f5_2 { 5 };

	int diff = f5 - f5_2;
	int zero = f5 - f5;

	std::cout << f5 + f5_2;
	std::cout << f5 - f5_2;
	std::cout << "diff" << diff << std::endl;
	std::cout << "zero" << zero << std::endl;

	int zero2 = f5 + (-f5);
	std::cout << "zero2" << zero2 << std::endl;

	std::cout << f5 * f5_2 << std::endl;
	std::cout << f5 / f5_2 << std::endl;
}
