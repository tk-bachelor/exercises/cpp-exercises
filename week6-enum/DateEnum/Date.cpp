#include "Date.h"
#include <stdexcept>
#include <ostream>
#include <iomanip>

#include <vector>

#include <string>

#include <array>

namespace date {

std::ostream& operator <<(std::ostream& out, Month m) {
	static std::vector<std::string> const month_map { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	out << month_map[m - 1];
	return out;
}

std::ostream& operator <<(std::ostream& out, Weekday d) {
	static std::vector<std::string> const weekday_map { "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday" };
	out << weekday_map[d];
	return out;
}

// ++month
Month operator++(Month& m) {
	m = Month((m % 12) + 1);
	return m;
}

// month++
Month operator++(Month& m, int) {
	Month old = m;
	++m;
	return old;
}

Date::Date(int year, Month month, int day) :
		year { year }, month { month }, day { day } {
	if (!isValidDate(year, month, day))
		throw std::out_of_range { "invalid date" };
}

Date::Date(Month month, int day, int year) :
		year { year }, month { month }, day { day } {
	if (!isValidDate(year, month, day))
		throw std::out_of_range { "invalid date" };
}

void Date::print(std::ostream& out) const {
	auto ch = out.fill('0');
	out << std::setw(2) << day << '.';
	out << std::setw(2) << month << '.';
	out << std::setw(4) << year;
	out.fill(ch);
}

bool Date::isValidYear(int year) {
	return year >= 1813 && year < 10000;
}

bool Date::isLeapYear(int year) {
	return !(year % 4) && ((year % 100) || !(year % 400));
}

bool Date::isValidDate(int year, Month month, int day) {
	return isValidYear(year) && day > 0 && day <= endOfMonth(year, month);
}

int Date::endOfMonth(int year, Month month) {
	switch (month) {
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
		return 31;
	case 4:
	case 6:
	case 9:
	case 11:
		return 30;
	case 2:
		return (isLeapYear(year) ? 29 : 28);
	}
	return 0;
}

void Date::nextDay() {
	if (day == endOfMonth(year, month)) {
		day = 1;
		if (month == 12) {
			month = jan;
			++year;
			if (!isValidYear(year))
				throw std::out_of_range("last year");
		} else {
			++month;
		}
	} else {
		++day;
	}
}

Weekday Date::dayOfWeek() const {
	static std::array<const int, 12> const t = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 };

	int y = year;
	int m = month;
	y -= m < 3;
	return Weekday((y + y / 4 - y / 100 + y / 400 + t[m - 1] + day + 6) % 7);
}

bool Date::operator <(Date const& rhs) const {
	return year < rhs.year || (year == rhs.year && (month < rhs.month || (month == rhs.month && day < rhs.day)));
}

}
