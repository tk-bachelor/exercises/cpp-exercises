#ifndef DATE_H_
#define DATE_H_

#include <iosfwd>

namespace date {

enum Month {
	jan = 1,
	feb,
	mar,
	apr,
	may,
	jun,
	jul,
	aug,
	sep,
	oct,
	nov,
	dec,
	january = jan,
	feburary,
	march,
	april,
	june = jun,
	july,
	august,
	september,
	october,
	november,
	december
};
std::ostream& operator<<(std::ostream& out, Month m);
Month operator++(Month& m);
Month operator++(Month& m, int);

enum Weekday {
	monday, tuesday, wednesday, thursday, friday, saturday, sunday
};
std::ostream& operator<<(std::ostream& out, Weekday d);

class Date {

public:
	Date(int year, Month month, int day);
	Date(Month month, int day, int year);
	void print(std::ostream& out) const;
	static bool isValidYear(int year);
	static bool isLeapYear(int year);
	static bool isValidDate(int year, Month month, int day);
	static int endOfMonth(int year, Month month);
	void nextDay();
	Weekday dayOfWeek() const;
	bool operator <(Date const& rhs) const;

private:
	int year { 9999 };
	Month month { Month::dec };
	int day { 31 };
};

inline std::ostream& operator<<(std::ostream& out, Date const& d) {
	d.print(out);
	return out;
}

inline bool operator>(Date const& lhs, Date const& rhs) {
	return rhs < lhs;
}
inline bool operator>=(Date const& lhs, Date const& rhs) {
	return !(lhs < rhs);
}
inline bool operator<=(Date const& lhs, Date const& rhs) {
	return !(rhs < lhs);
}

inline bool operator==(Date const& lhs, Date const& rhs) {
	return !(lhs < rhs) && !(rhs < lhs);
}
inline bool operator!=(Date const& lhs, Date const& rhs) {
	return !(lhs == rhs);
}

}

#endif /* DATE_H_ */
