#include "Date.h"

#include <iostream>

int main() {
	using namespace date;
	Date d { 1994, Month::apr, 13 };
	std::cout << d << std::endl;

	Date d2 { Month::nov, 11, 2017 };
	std::cout << "Weekday: " << d2.dayOfWeek();
}
