#include<iostream>

struct Animal {

	Animal() {
		std::cout << "animal born\n";
	}

	~Animal() {
		std::cout << "animal died\n";
	}

	void makeSound() {
		std::cout << "---\n";
	}

	virtual void move() {
		std::cout << "---\n";
	}
};

struct Bird: Animal {

	Bird() {
		std::cout << "bird hatched\n";
	}

	~Bird() {
		std::cout << "bird crashed\n";
	}

	virtual void makeSound() {
		std::cout << "chirp\n";
	}

	void move() {
		std::cout << "fly\n";
	}
};

struct HummingBird: Bird {

	HummingBird() {
		std::cout << "hummingbird hatched\n";
	}

	~HummingBird() {
		std::cout << "HummingBird died\n";
	}

	void makeSound() {
		std::cout << "peep\n";
	}

	void move() {
		std::cout << "hum\n";
	}
};

int main() {
	std::cout << "(a)----------------------------\n";
	HummingBird hummingbird; // animal born, bird hatched, hummingbird hatched
	Bird bird = hummingbird; // no logs because copy-construct is called
	Animal & animal = hummingbird; // no logs of because of reference passing
	std::cout << "(b)-----------------------------\n";
	hummingbird.makeSound(); // peep
	bird.makeSound(); // chirp --> even though it is virtual.. because bird is copied and doesn't know of hummingbird anymore
	animal.makeSound(); // --- because makeSound is NOT virtual
	std::cout << "(c)-----------------------------\n";
	hummingbird.move(); // hum
	bird.move(); // fly
	animal.move(); // hum --> move is virtual in Animal
	std::cout << "(d)-----------------------------\n";
	// destruction
	// &animal will be skipped because of reference
	// bird destructor --> bird crashed, animal died
	// hummingBird destructor --> Hummingbird died, bird crashed, animal died
}

