#include <iostream>

struct Base {
	virtual void function() const {
		std::cout << "Base\n";
	}

	void process(int i) {
		std::cout << "Base " << i << "\n";
	}
};

struct Derived: Base {
	using Base::process;
	void function() const {
		std::cout << "Derived \n";
	}

	void process(float f) {
		std::cout << "Derived " << f << "\n";
	}
};

void call_function(Base& b) {
	b.function();
}

int main() {
	Derived d { };
	call_function(d);
	Base b { };
	call_function(b);

	int i = 5;
	d.process(i);
}
