# Dynamic Polymorphism

### Output

**section a** - create objects
```cpp
// create forum_troll
a monster is bread
a troll grows
not quite a monster
// create a troll by copying forum_troll -- no constructor is called
// create a monster reference from forum_troll -- no new object is created
```

**section b** - attack
```cpp
// ft.attack() from forum_troll
write stupid things
// t.attack() from troll and health = 9
clubbing kills me
// m.attack() from forum_troll because the method is virtual in monster
write stupid things
```

**section c** - swing club
```cpp
// ft.swing_club() from forum_troll and health=11
swinging is healthy
// t.swing_club() from troll and health=8
clubbing kills me
```

**section d** - health
```cpp
// ft.health from troll
troll-health: 11
// t.health from troll
troll-health: 8
// m.health from monster because it is not virtual
immortal?
```

**section end** - destructors
```cpp
// destruct m ref--> ignored
// destruct t copy --> destruct t, m
troll petrified
monster killed
// destruct ft --> destruct ft, t, m
troll banned
troll petrified
monster killed
```