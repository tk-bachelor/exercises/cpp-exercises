#include "Inheritance.h"

int main(){
	cout << "a ------\n";
	forum_troll ft{};
	troll t{ft} ;
	monster &m{ft};
	cout << "b ------\n";
	ft.attack();
	t.attack(); // health 9
	m.attack();
	cout << "c ------\n";
	ft.swing_club(); // health 11
	t.swing_club(); // health 8
	cout << "d ------\n";
	ft.health(); // 11
	t.health(); // 8
	m.health(); // immortal
	cout << "end ------\n";
}
