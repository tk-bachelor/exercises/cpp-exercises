#ifndef SEVEN_SEGMENT_H_
#define SEVEN_SEGMENT_H_

#include <iosfwd>
#include <vector>

void printLargeDigit(unsigned i, std::ostream &out);
void printLargeNumber(unsigned i, std::ostream &out);
int getNumberOfDigits(unsigned number);
std::vector<unsigned> getDigits(unsigned number);

#endif /* SEVEN_SEGMENT_H_ */
