#ifndef CHARCOUNT_H_
#define CHARCOUNT_H_

#include <iosfwd>

unsigned charcount(std::istream& input);
unsigned allcharcount(std::istream& input);
unsigned wordcount(std::istream& input);
unsigned acount(std::istream& input);
unsigned linecount(std::istream& input);

#endif /* CHARCOUNT_H_ */
