#include "charcount.h"
#include <istream>
#include <iterator>

#include <string>
#include <algorithm>

unsigned charcount(std::istream& input) {
	using in=std::istream_iterator<char>;
	return std::distance(in { input }, in { });
}

unsigned allcharcount(std::istream& input) {
	using in = std::istreambuf_iterator<char>;
	return std::distance(in { input }, in { });
}

unsigned wordcount(std::istream& input) {
	using in=std::istream_iterator<std::string>;
	return std::distance(in { input }, in { });
}

unsigned linecount(std::istream& input) {
	using in=std::istreambuf_iterator<char>;
	return std::count(in { input }, in { }, '\n');
}

unsigned acount(std::istream& input) {
	using in=std::istream_iterator<char>;
	return std::count(in { input }, in { }, 'a');
}
