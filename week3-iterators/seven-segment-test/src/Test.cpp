#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include "seven-segment.h"

#include <sstream>

#include <vector>

void testPrintLargeDigitZero() {
	std::ostringstream output { };
	printLargeDigit(0, output);
	ASSERT_EQUAL(" - \n"
			"| |\n"
			"   \n"
			"| |\n"
			" - \n", output.str());
}

void testGetDigits() {
	std::vector<unsigned> digits { 1, 3, 4, 5 };
	ASSERT_EQUAL(digits, getDigits(1345));
}

void testPrintLargeNumberThirteen() {
	std::ostringstream output { };
	printLargeNumber(13, output);
	ASSERT_EQUAL(
			"    - \n"
			"  |  |\n"
			"    - \n"
			"  |  |\n"
			"    - \n", output.str());
}

void testGetNumberOfDigits() {
	ASSERT_EQUAL(4, getNumberOfDigits(1314));
}
void testGetNumberOfDigitsWithZero() {
	ASSERT_EQUAL(1, getNumberOfDigits(0));
}

bool runAllTests(int argc, char const *argv[]) {
	cute::suite s { };

	s.push_back(CUTE(testPrintLargeDigitZero));
	s.push_back(CUTE(testGetNumberOfDigits));
	s.push_back(CUTE(testGetDigits));
	s.push_back(CUTE(testPrintLargeNumberThirteen));
	s.push_back(CUTE(testGetNumberOfDigitsWithZero));

	cute::xml_file_opener xmlfile(argc, argv);
	cute::xml_listener<cute::ide_listener<>> lis(xmlfile.out);
	auto runner = cute::makeRunner(lis, argc, argv);
	bool success = runner(s, "AllTests");
	return success;
}

int main(int argc, char const *argv[]) {
	return runAllTests(argc, argv) ? EXIT_SUCCESS : EXIT_FAILURE;
}
