#include <iterator>

#include <numeric>

#include <iostream>

#include <vector>

int sumi(std::istream& input){
	int total = std::accumulate(std::istream_iterator<int>(input), std::istream_iterator<int>(), 0);
	return total;
}

int sumi(std::vector<int> numbers){
	return std::accumulate(numbers.begin(), numbers.end(), 0);
}

int main() {
//	std::cout << sumi(std::cin);

	std::vector<int> numbers{1,3,5};
	std::cout << sumi(numbers);
	return 0;
}
