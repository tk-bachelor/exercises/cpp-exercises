//module.cpp
#include "module.h"

#include <sstream>
#include <iterator>

namespace {

module_ptr get_module_ptr(std::string const & module_name, module_catalog & catalog) {
	if (catalog.count(module_name) == 0) {
		auto m = std::make_shared<module>(module_name);
		catalog[module_name] = m;
	}
	return catalog[module_name];
}

void add(module_catalog& catalog, std::string const & line) {
	std::stringstream line_stream{line};
	std::string module_name;
	line_stream >> module_name;
	auto module = get_module_ptr(module_name, catalog);
	std::for_each(std::istream_iterator<std::string>{line_stream}, std::istream_iterator<std::string>{}, [&](std::string const & module_name){
		auto dependency = get_module_ptr(module_name, catalog);
		module->add_predecessor(dependency);
	});
}

}

module_catalog read_modules(std::istream & input) {
	module_catalog catalog{};
	std::string line;
	while (getline(input, line)) {
		add(catalog, line);
	}
	return catalog;
}
