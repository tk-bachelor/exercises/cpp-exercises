//module.h
#ifndef MODULE_H_
#define MODULE_H_

#include <algorithm>
#include <iosfwd>
#include <iterator>
#include <map>
#include <memory>
#include <string>
#include <vector>

struct module;

using module_ptr = std::shared_ptr<module>;
using modules = std::vector<module_ptr>;
using module_catalog = std::map<std::string, module_ptr>;

struct module : std::enable_shared_from_this<module> {
	explicit module(std::string const & name) : name {name}{};

	void add_predecessor(module_ptr predecessor) {
		std::weak_ptr<module> wp{predecessor};
		_predecessors.push_back(wp);
		predecessor->_successors.push_back(shared_from_this());
	}

	bool has_predecessor() const {
		return !_predecessors.empty();
	}

	void update_earliest_semester(unsigned newEarliestSemester) {
		earliest_semester = std::max(newEarliestSemester, earliest_semester);
	}

	unsigned get_earliest_semester() const {
		return earliest_semester;
	}

	modules successors() const {
		return _successors;
	}

	modules predecessors() {
		remove_expired_predecessors();
		modules predecessors_shared{_predecessors.size()};
		std::transform(std::begin(_predecessors), std::end(_predecessors), std::begin(predecessors_shared), [](module_weak_ptr m) {
			return m.lock();
		});
		return predecessors_shared;
	}

	std::string const name;
private:
	using module_weak_ptr = std::weak_ptr<module>;
	using module_back_references = std::vector<module_weak_ptr>;

	void remove_expired_predecessors() {
		auto removed_begin = std::remove_if(std::begin(_predecessors), std::end(_predecessors), [](module_weak_ptr m) {
			return m.expired();
		});
		_predecessors.erase(removed_begin, std::end(_predecessors));
	}

	unsigned earliest_semester{0};
	module_back_references _predecessors{};
	modules _successors{};

};


inline std::ostream & operator<<(std::ostream& out, module_ptr m) {
	out << m->name;
	return out;
}

module_catalog read_modules(std::istream & input);

template<typename M>
inline std::vector<typename M::mapped_type> values(M const& catalog) {
	std::vector<typename M::mapped_type> values{catalog.size()};
	std::transform(std::begin(catalog), std::end(catalog), std::begin(values),[](typename M::value_type const & pair){
		return pair.second;
	});
	return values;
}



#endif /* MODULE_H_ */
