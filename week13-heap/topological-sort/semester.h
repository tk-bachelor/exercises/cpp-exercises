//semester.h
#ifndef SEMESTER_H_
#define SEMESTER_H_

#include <iosfwd>

void print_semester(std::istream& in, std::ostream& out);

#endif /* SEMESTER_H_ */
