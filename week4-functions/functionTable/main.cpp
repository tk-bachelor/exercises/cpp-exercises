#include <ostream>

#include <vector>

#include <iterator>

#include <algorithm>

#include <iostream>

void printFunctionTable(std::ostream& out, double begin, double end, int steps, double func(double)) {
	std::vector<double> x(steps);
	double const delta { (end - begin) / (steps - 1) };

	//	for (int i = 0; i < steps; i++) {
	//		x[i] = begin + delta * i;
	//	}
	generate(x.begin(), x.end(), [begin, delta, i=0] () mutable {
				return begin + (i++) * delta;
			});

	out << "x\t";
	std::ostream_iterator<double> it { out, "\t" };
	copy(x.begin(), x.end(), it);

	out << "\nf(x)\t";
	transform(x.begin(), x.end(), it, func);
	out << "\n";
}

int main() {
	printFunctionTable(std::cout, 1, 10, 10, [](double x) {return x * x;});
}
