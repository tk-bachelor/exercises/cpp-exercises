#include <iterator>

#include <string>

#include <vector>

#include <iostream>

#include <algorithm>

#include <cctype>

bool caseless_less(std::string const &left, std::string const &right) {
	return std::lexicographical_compare(begin(left), end(left), begin(right), end(right), [](char lc, char rc) {
		return std::tolower(lc) < std::tolower(rc);
	});
}

bool caseless_equal(std::string const &left, std::string const &right){
	return std::equal(begin(left), end(left), begin(right), end(right), [](char lc, char rc){
		return std::tolower(lc) == std::tolower(rc);
	});
}

int main() {
	using in=std::istream_iterator<std::string>;
	std::vector<std::string> wlist { in { std::cin }, in { } };

	std::sort(wlist.begin(), wlist.end(), caseless_less);

	wlist.erase(unique(begin(wlist), end(wlist), caseless_equal), wlist.end());

	copy(wlist.begin(), wlist.end(), std::ostream_iterator<std::string>{std::cout, "\n"});
}
