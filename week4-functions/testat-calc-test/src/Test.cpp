#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include "calc.h"
#include "sevensegment.h"

#include <stdexcept>
#include <string>
#include <sstream>

#include <vector>

void testCalcOnePlusOne() {
	auto result = calc(1, 1, '+');
	ASSERT_EQUAL(2, result);
}

void testCalcTwoMinusNegativeOne() {
	auto result = calc(2, -1, '-');
	ASSERT_EQUAL(3, result);
}

void testCalcMinusThreeTimesMinusTwo() {
	auto result = calc(-3, -2, '*');
	ASSERT_EQUAL(6, result);
}

void testCalcSevenDividedByTwo() {
	auto result = calc(7, 2, '/');
	ASSERT_EQUAL(3, result);
}

void testCalcSevenDividedByZero() {
	ASSERT_THROWS(calc(7, 0, '/'), std::invalid_argument);
}

void testCalcSevenModuloTwo() {
	auto result = calc(7, 2, '%');
	ASSERT_EQUAL(1, result);
}

void testCalcSevenModuloByZero() {
	ASSERT_THROWS(calc(7, 0, '%'), std::invalid_argument);
}

void testCalcUnsupportedOperation() {
	ASSERT_THROWS(calc(7, 2, '^'), std::invalid_argument);
}

void testCalcExpressionAddition() {
	std::istringstream in { "7+2" };
	auto result = calc(in);
	ASSERT_EQUAL(9, result);
}

void testCalcExpressionSubstraction() {
	std::istringstream in { "7-2" };
	auto result = calc(in);
	ASSERT_EQUAL(5, result);
}

void testCalcExpressionMultiplication() {
	std::istringstream in { "7*2" };
	auto result = calc(in);
	ASSERT_EQUAL(14, result);
}

void testCalcExpressionDivision() {
	std::istringstream in { "7/2" };
	auto result = calc(in);
	ASSERT_EQUAL(3, result);
}

void testCalcBadExpressionDivision() {
	std::istringstream in { "7 / 0" };
	ASSERT_THROWS(calc(in), std::invalid_argument);
}

void testCalcExpressionModulo() {
	std::istringstream in { "7 % 2" };
	auto result = calc(in);
	ASSERT_EQUAL(1, result);
}

void testCalcExpressionBadOperand() {
	std::istringstream in { "7 + s" };
	ASSERT_THROWS(calc(in), std::invalid_argument);
}

void testCalcExpressionBadOperator() {
	std::istringstream in { "7 ^ 2" };
	ASSERT_THROWS(calc(in), std::invalid_argument);
}

void testPrintLargeDigitZero() {
	std::ostringstream output { };
	printLargeDigit(0, output);
	ASSERT_EQUAL(""
			" - \n"
			"| |\n"
			"   \n"
			"| |\n"
			" - \n", output.str());
}

void testPrintLargeDigitsZeroToNine() {
	std::ostringstream output { };
	printLargeNumber(1234567890, output);
	ASSERT_EQUAL(""
			"    -  -     -  -  -  -  -  - \n"
			"  |  |  || ||  |    || || || |\n"
			"    -  -  -  -  -     -  -    \n"
			"  ||    |  |  || |  || |  || |\n"
			"    -  -     -  -     -  -  - \n", output.str());
}

void testGetDigits() {
	std::vector<unsigned> digits { 1, 3, 4, 5 };
	ASSERT_EQUAL(digits, getDigits(1345));
}

void testGetDigitsZero() {
	std::vector<unsigned> digits { 0 };
	ASSERT_EQUAL(digits, getDigits(0));
}

void testGetDigitsHundred() {
	std::vector<unsigned> digits { 1,0,0 };
	ASSERT_EQUAL(digits, getDigits(100));
}

void testPrintLargeNumberThirteen() {
	std::ostringstream output { };
	printLargeNumber(13, output);
	ASSERT_EQUAL("    - \n"
			"  |  |\n"
			"    - \n"
			"  |  |\n"
			"    - \n", output.str());
}

void testGetNumberOfDigits() {
	ASSERT_EQUAL(4, getNumberOfDigits(1314));
}

void testGetNumberOfDigitsWithZero() {
	ASSERT_EQUAL(1, getNumberOfDigits(0));
}

bool runAllTests(int argc, char const *argv[]) {
	cute::suite s { };

	s.push_back(CUTE(testCalcOnePlusOne));
	s.push_back(CUTE(testCalcTwoMinusNegativeOne));
	s.push_back(CUTE(testCalcMinusThreeTimesMinusTwo));
	s.push_back(CUTE(testCalcSevenDividedByTwo));
	s.push_back(CUTE(testCalcSevenDividedByZero));
	s.push_back(CUTE(testCalcSevenModuloTwo));
	s.push_back(CUTE(testCalcSevenModuloByZero));
	s.push_back(CUTE(testCalcUnsupportedOperation));

	s.push_back(CUTE(testCalcExpressionAddition));
	s.push_back(CUTE(testCalcExpressionSubstraction));
	s.push_back(CUTE(testCalcExpressionMultiplication));
	s.push_back(CUTE(testCalcExpressionDivision));
	s.push_back(CUTE(testCalcExpressionModulo));

	s.push_back(CUTE(testCalcExpressionBadOperand));
	s.push_back(CUTE(testCalcExpressionBadOperator));
	s.push_back(CUTE(testPrintLargeDigitZero));
	s.push_back(CUTE(testGetDigits));
	s.push_back(CUTE(testPrintLargeNumberThirteen));
	s.push_back(CUTE(testGetNumberOfDigits));
	s.push_back(CUTE(testGetNumberOfDigitsWithZero));
	s.push_back(CUTE(testPrintLargeDigitsZeroToNine));
	s.push_back(CUTE(testCalcBadExpressionDivision));
	s.push_back(CUTE(testGetDigitsZero));
	s.push_back(CUTE(testGetDigitsHundred));

	cute::xml_file_opener xmlfile(argc, argv);
	cute::xml_listener<cute::ide_listener<>> lis(xmlfile.out);
	auto runner = cute::makeRunner(lis, argc, argv);
	bool success = runner(s, "AllTests");
	return success;
}

int main(int argc, char const *argv[]) {
	return runAllTests(argc, argv) ? EXIT_SUCCESS : EXIT_FAILURE;
}
