#ifndef CALC_H_
#define CALC_H_

#include <iosfwd>

int calc(int lhs, int rhs, char op);
int calc(std::istream& input);

#endif
