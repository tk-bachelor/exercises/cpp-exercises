#include "calc.h"
#include <stdexcept>
#include <istream>

int calc(int lhs, int rhs, char op) {
	switch (op) {
	case '+':
		return lhs + rhs;
	case '-':
		return lhs - rhs;
	case '*':
		return lhs * rhs;
	case '/':
		if (rhs == 0) {
			throw std::invalid_argument("Division by zero");
		}
		return lhs / rhs;
	case '%':
		if (rhs == 0) {
			throw std::invalid_argument("Division by zero");
		}
		return lhs % rhs;
	default:
		throw std::invalid_argument("Unsupported operation");
	}
}

int calc(std::istream& input) {
	char op { };
	int left { }, right { };

	if(input >> left >> op >> right){
		return calc(left, right, op);
	}
	throw std::invalid_argument("Invalid calculation");
}

