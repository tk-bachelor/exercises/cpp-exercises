#ifndef POCKETCALCULATOR_H_
#define POCKETCALCULATOR_H_

#include <iosfwd>

void run(std::istream& input, std::ostream &out);

#endif /* POCKETCALCULATOR_H_ */
