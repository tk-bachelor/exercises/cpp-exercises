#include "pocketcalculator.h"
#include "sevensegment.h"
#include <src/calc.h>
#include <ostream>

#include <stdexcept>

#include <string>

#include <sstream>

void run(std::istream& input, std::ostream &out) {
	while (input) {
		try {
			out << "=";
			std::string line { };
			std::getline(input, line);

			std::istringstream in { line };
			printLargeNumber(calc(in), out);
			out << "\n";
		} catch (const std::invalid_argument &ex) {
			printError(out);
			out << ex.what() << "\n";
		}
	}
}
