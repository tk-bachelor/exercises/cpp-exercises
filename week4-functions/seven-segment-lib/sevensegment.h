#ifndef SEVENSEGMENT_H_
#define SEVENSEGMENT_H_

#include <iosfwd>
#include <vector>

void printLargeDigit(unsigned i, std::ostream &out);
void printLargeNumber(unsigned i, std::ostream &out);
void printError(std::ostream &out);
int getNumberOfDigits(unsigned number);
std::vector<unsigned> getDigits(unsigned number);


#endif /* SEVENSEGMENT_H_ */
