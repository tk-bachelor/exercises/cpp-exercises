#include "sevensegment.h"
#include <vector>
#include <string>
#include <stdexcept>
#include <algorithm>

#include <iterator>

const std::vector<std::string> zero = { " - ", "| |", "   ", "| |", " - " };
const std::vector<std::string> one = { "   ", "  |", "   ", "  |", "   " };
const std::vector<std::string> two = { " - ", "  |", " - ", "|  ", " - " };
const std::vector<std::string> three = { " - ", "  |", " - ", "  |", " - " };
const std::vector<std::string> four = { "   ", "| |", " - ", "  |", "   " };
const std::vector<std::string> five = { " - ", "|  ", " - ", "  |", " - " };
const std::vector<std::string> six = { " - ", "|  ", " - ", "| |", " - " };
const std::vector<std::string> seven = { " - ", "  |", "   ", "  |", "   " };
const std::vector<std::string> eight = { " - ", "| |", " - ", "| |", " - " };
const std::vector<std::string> nine = { " - ", "| |", " - ", "  |", " - " };
const std::vector<std::vector<std::string>> digits { zero, one, two, three, four, five, six, seven, eight, nine };

unsigned const segmentSize = zero.size();

void printNewline(std::ostream &out) {
	out << '\n';
}

void printline(std::ostream &out, std::string str) {
	out << str << '\n';
}

void printLargeDigit(unsigned digit, std::ostream &out) {
	if (digit >= digits.size()) {
		throw std::invalid_argument("Digit should be below 10");
	}
	std::vector<std::string> vDigit = digits.at(digit);
//	for (auto digitSegment : vDigit) {
//		out << digitSegment;
//		printNewline(out);
//	}
	std::copy(vDigit.begin(), vDigit.end(), std::ostream_iterator<std::string>{out, "\n"});
}

int getNumberOfDigits(unsigned number) {
	unsigned i { number };
	unsigned noOfDigits { 0 };

	if (i == 0) {
		return 1;
	}

	if (i < 0) {
		i = number * -1;
	}

	while (i > 0) {
		i = i / 10;
		noOfDigits++;
	}
	return noOfDigits;
}

std::vector<unsigned> getDigits(unsigned number) {
	std::vector<unsigned> extractedDigits { };
	unsigned cNumber { number };

	if(cNumber == 0){
		extractedDigits.push_back(0);
		return extractedDigits;
	}

	while (cNumber > 0) {
		extractedDigits.push_back(cNumber % 10);
		cNumber = cNumber / 10;
	}
	std::reverse(std::begin(extractedDigits), std::end(extractedDigits));
	return extractedDigits;
}

void printLargeNumber(unsigned i, std::ostream &out) {
	std::vector<unsigned> extractedDigits = getDigits(i);

	for (unsigned pos = 0; pos < segmentSize; pos++) {
		for (auto digit : extractedDigits) {
			out << digits.at(digit).at(pos);
		}
		printNewline(out);
	}
}

void printError(std::ostream &out) {
	out << " - \n"
			"|  \n"
			" -  -  -  -  - \n"
			"|  |  |  | || \n"
			" �        � \n";
}
