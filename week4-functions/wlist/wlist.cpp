#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>

#include <set>

int main() {

	using in=std::istream_iterator<std::string>;
	std::set<std::string> wlist{in{std::cin}, in{}};

	copy(begin(wlist), end(wlist), std::ostream_iterator<std::string>{std::cout, "\n"});
	return 0;
}
