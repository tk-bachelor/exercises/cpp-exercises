# Week 2 - Values

https://wiki.ifs.hsr.ch/CPlusPlus/ExW2

## Exercise 1 - Questions about the Lecture

**Please figure out, in which standard header file the following elements of namespace std are declared. Also point out, if it is a variable, function or type. Please note, in a concrete compiler implementation the exact place might be in another header file than the official standard one. You should provide the latter. Consult provided online references, if in doubt and to check your answers. But first try Cevelop's means (experiment with CTRL-click and the include browser!). If there is a difference in the position where the element is defined according to Cevelop and the standard documentation give both headers and try to explain it!**

| elements of `std` | standard header file | variable / function / type | Description |
| -- | -- | -- | -- |
| std::cout | iostream | global variable | controls output to the stream |
| std::cin | iostream | global variable | The global object `std::cin` controls input from a stream buffer associated with the standard C input stream stdin. |
| std::endl | ostream | function | Inserts a newline character into the output sequence os and flushes it
| std::tolower | cctype | function | Converts to lowercase
| std::string | string | type | sequence of characters
| std::distance | iterator | function | Returns the number of hops from first to last.
| std::istream_iterator | iterator | function | a single-pass input iterator that reads successive objects of type T from the std::basic_istream object for which it was constructed, by calling the appropriate operator>>. 
| std::size_t | cstddef | type | std::size_t is the unsigned integer type of the result of the sizeof operator
| std::vector | vector | type | is a sequence container that encapsulates dynamic size arrays. |

**What are the types of the following literals:**

| literal value | type |
| -- | -- |
| `45` | int |
| `0XDULL` | unsigned long long int |
| `5.75` | double |
| `.2f` | float |
| `"string"` | const char * |
| `' '` | char |

**What is the state (failbit, eofbit, badbit) of the of the input stream std::cin after reading into i in the following code? For the inputs below:**

```cpp
int main() {
  int i{};
  std::cin >> i;
  //...
}
```

Inputs:
* 10 => good. i = 10
* a => failbit. i = 0
* `<no input>` => eofbit. program awaits until a character is entered.

more info: https://stackoverflow.com/questions/11085151/c-ifstream-failbit-and-badbit