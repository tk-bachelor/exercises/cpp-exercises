#include <iostream>

int sumi(std::istream &in) {
	int sum { 0 };
	int number { };
	while (in >> number) {
		sum += number;
	}
	std::cout << "Sum: " << sum << "\n";
	return sum;
}

int sumf(std::istream &in) {
	float sum { 0 };
	float number { };
	while (in >> number) {
		sum += number;
	}
	std::cout << "Sum: " << sum << "\n";
	return sum;
}

int averagei(std::istream &in) {
	int sum { 0 };
	int counter { 0 };
	int number { };
	while (in >> number) {
		sum += number;
		counter++;
	}
	int average = sum / counter;
	std::cout << "Average: " << average << "\n";
	return average;
}

int averagef(std::istream &in) {
	float sum { 0 };
	unsigned counter { 0 };
	float number { };
	while (in >> number) {
		sum += number;
		counter++;
	}
	float average = sum / counter;
	std::cout << "Average: " << average << "\n";
	return average;
}

void printnice(std::ostream &out, unsigned const i) {
	out.width(5);
	out << i;
}

void printheader(std::ostream &out, unsigned const limit) {
	printnice(out, 0);
	for (unsigned i = 1; i <= limit; i++) {
		printnice(out, i);
	}
	out << "\n";
}

void multab(std::istream &in, std::ostream &out) {
	unsigned limit { };
	while (in.good() && (limit < 1 || limit > 30)) {
		out << "Enter limit (max 30):";
		in >> limit;
	}
	limit = (limit < 1 || limit > 30) ? 10 : limit;

	printheader(out, limit);
	for (unsigned i = 1; i <= limit; i++) {
		printnice(out, i);
		for (auto j = 1u; j <= limit; j++) {
			printnice(out, i * j);
		}
		out << "\n";
	}
}

int main() {
//	sumi(std::cin);
//	sumf(std::cin);
//	averagei(std::cin);
//	averagef(std::cin);
	multab(std::cin, std::cout);
}
