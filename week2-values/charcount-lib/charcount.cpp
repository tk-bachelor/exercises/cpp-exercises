#include "charcount.h"
#include <istream>
#include <string>

unsigned charc(std::istream& input) {
	unsigned count { };
	char c { };
	while (input >> c) {
		count++;
	}
	return count;
}

unsigned allcharc(std::istream& input) {
	unsigned count { };
	char c { };
	while (input.get(c)) {
		if (c != '\n') {
			count++;
		}
	}
	return count;
}

unsigned wc(std::istream& input) {
	unsigned count { };
	std::string word { };
	while (input >> word) {
		count++;
	}
	return count;
}

unsigned lc(std::istream& input) {
	unsigned count { };
	char c { };
	while (input.get(c)) {
		if (c == '\n') {
			count++;
		}
	}
	return count;
}

