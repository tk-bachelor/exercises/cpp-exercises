#ifndef CHARCOUNT_H_
#define CHARCOUNT_H_

#include <iosfwd>

unsigned charc(std::istream& input);
unsigned allcharc(std::istream& input);
unsigned wc(std::istream& input);
unsigned lc(std::istream& input);

#endif /* CHARCOUNT_H_ */
