#include <iostream>
#include <string>

void askForName(std::ostream &out) {
	out << "What is your name? ";
}

std::string inputName(std::istream &in) {
	std::string name { };
	std::getline(in, name);
	return name;
}

void sayGreeting(std::ostream &out, std::string name) {
	out << "Hello " << name << ", how are you? \n";
}

int main() {

	bool cond{};
	int i;
	std::cout << "Number:";

	std::cin >> i;
	cond = i % 2;
	std::cout << (cond ? "Hello" : "Peter") << i << "\n";

	askForName(std::cout);
	sayGreeting(std::cout, inputName(std::cin));
}
