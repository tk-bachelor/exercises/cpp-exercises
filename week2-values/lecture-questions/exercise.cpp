#include <iostream>
#include <ostream>
#include <cctype>
#include <string>
#include <iterator>

#include <vector>
#include <sstream>
#include <numeric>

#include <cstddef>

int main() {
	std::cout << "Questions about lecture" << std::endl;
	std::cout << "C is " << (char) std::tolower('C') << std::endl;

	std::string name { "Keerthikan" };
	std::cout << "My name is " << name << std::endl;

	std::vector<int> v { 3, 1, 4 };
	int d = std::distance(v.begin(), v.end());
	std::cout << "distance of v begin to end is " << d << std::endl;
	std::cout << "distance of v end to begin is "
			<< std::distance(v.end(), v.begin()) << std::endl;

	std::istringstream str("0.1 0.2 0.3 0.4");
	std::partial_sum(std::istream_iterator<double>(str),
			std::istream_iterator<double>(),
			std::ostream_iterator<double>(std::cout, " "));

	const std::size_t N = 10;
	std::cout << "size_t N is " << N << std::endl;

	int n{};
	std::cin >> n;
	std::cout << "n = " << n << std::endl;

	std::cout << "Stream input states" << std::endl;
	std::cout << "eofbit:" << std::cin.eof() << std::endl;
	std::cout << "failbit:" << std::cin.fail() << std::endl;
}
