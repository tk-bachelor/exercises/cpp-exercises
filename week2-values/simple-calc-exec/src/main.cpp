#include <calculator.h>
#include <iostream>
#include <stdexcept>

int main() {
	while (std::cin) {
		try {
			std::cout << '=' << calc(std::cin) << '\n';
		} catch (const std::invalid_argument &ex) {
			std::cerr << "Error occurred: ";
			std::cerr << ex.what() << "\n";
		}
	}
}
