#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include "charcount.h"

#include <string>
#include <sstream>

void testCharc() {
	std::istringstream in { "Hello world" };
	unsigned result = charc(in);
	ASSERT_EQUAL(10, result);
}

void testAllcharc() {
	std::istringstream in { "Hello world" };
	unsigned result = allcharc(in);
	ASSERT_EQUAL(11, result);
}

void testWc() {
	std::istringstream in { "Hello world" };
	unsigned result = wc(in);
	ASSERT_EQUAL(2, result);
}

void testLc() {
	std::istringstream in { "Hello world\nBye World\n" };
	unsigned result = lc(in);
	ASSERT_EQUAL(2, result);
}

bool runAllTests(int argc, char const *argv[]) {
	cute::suite s { };

	s.push_back(CUTE(testCharc));
	s.push_back(CUTE(testAllcharc));
	s.push_back(CUTE(testWc));
	s.push_back(CUTE(testLc));

	cute::xml_file_opener xmlfile(argc, argv);
	cute::xml_listener<cute::ide_listener<>> lis(xmlfile.out);
	auto runner = cute::makeRunner(lis, argc, argv);
	bool success = runner(s, "AllTests");
	return success;
}

int main(int argc, char const *argv[]) {
	return runAllTests(argc, argv) ? EXIT_SUCCESS : EXIT_FAILURE;
}
