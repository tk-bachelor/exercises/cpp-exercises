#ifndef SRC_CALCULATOR_H_
#define SRC_CALCULATOR_H_

#include <iosfwd>

int calc(int lhs, int rhs, char op);
int calc(std::istream& input);

#endif
