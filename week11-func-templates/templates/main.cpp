#include <iostream>

#include <string>

#include <algorithm>

#include <vector>

#include <iterator>

#include <numeric>

#include <functional>

#include <sstream>

template<typename T>
T const &median(T const &a, T const &b, T const &c){
	T abMin = std::min(a, b);
	T abMax = std::max(a, b);
	T abMaxCMin = std::min(abMax, c);
	return std::max(abMin, abMaxCMin);
}

std::string median(char const *ap, char const *bp, char const *cp){
	std::string a {ap};
	std::string b {bp};
	std::string c {cp};
	return median(a,b,c);
}

template<typename T>
void rotate3arguments(T &a, T &b, T &c){
	std::swap(a, b);
	std::swap(a, c);
}

template<typename ...T>
void doRead(std::istream &in, T& ...vars);

template<typename ...T>
void doRead(std::istream &in, std::string &rest){
	getline(in, rest);
}

template<typename HEAD, typename ...T>
void doRead(std::istream &in, HEAD &h, T&... vars){
	in >> h;
	if(sizeof...(T)){
		doRead(in, vars...);
	}
}

template<typename ...T>
void readIn(std::istream &in, T&... vars){
	std::string line{};
	getline(in, line);
	std::istringstream input {line};

	doRead(input, vars...);
	if(input.fail()){
		in.setstate(std::ios::failbit);
	}
}

int main(){

	std::vector<unsigned> v(10, 1);
	std::iota(v.begin(), v.end(), 1);
	std::transform(v.begin(), v.end(), v.begin(), v.begin(), std::multiplies<>{});
	std::copy(v.begin(), v.end(), std::ostream_iterator<unsigned>{std::cout, " "});

	std::cout << "Function templates\n";

	std::cout << "median\n";
	std::cout << median("Hello", "World", "Bye");
	std::cout << median(3, 1, 2);

	std::cout << "rotate\n";
	int a = 1, b = 2, c = 3;
	std::string as{"PrFm"}, bs{"MGE"}, cs{"Eng:HTW"};
	rotate3arguments(a, b, c);
	std::cout << a << b << c << "\n";
	rotate3arguments(as, bs, cs);
	std::cout << as << bs << cs << "\n";

	std::string rest{};
	int first{};
	int second{};

	readIn(std::cin, first, second, rest);
	std::cout << "first: " << first << "\n";
	std::cout << "second: " << second << "\n";
	std::cout << "rest: " << rest << "\n";
}
